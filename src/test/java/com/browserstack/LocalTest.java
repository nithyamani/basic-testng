package com.browserstack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.browserstack.local.Local;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class LocalTest{
    private static Local l;
    public WebDriver driver;
    public static final String USERNAME = "nithyamani3";
    public static final String AUTOMATE_KEY = "P4JKysg5WuchQxBfKQu1";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    public static void main(String args[]) throws Exception {
        DesiredCapabilities caps = new DesiredCapabilities();

        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "62.0");

        caps.setCapability("build","testng-browserstack1");
        caps.setCapability("name","local_test");
        caps.setCapability("browserstack.debug", "true");

        caps.setCapability("browserstack.local", "true");

        //Enabling Local through code bindings
        if (caps.getCapability("browserstack.local") != null
                && caps.getCapability("browserstack.local") == "true") {
            l = new Local();
            Map<String, String> options = new HashMap<String, String>();
            options.put("key", AUTOMATE_KEY);
            l.start(options);
        }

        WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
        driver.get("http://localhost:8000/");
        System.out.println(driver.getTitle());

        driver.quit();
        if (l != null) {
            l.stop();
        }

    }
}
