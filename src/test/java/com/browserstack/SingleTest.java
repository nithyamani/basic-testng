package com.browserstack;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

import java.net.URL;
import java.util.Set;

public class SingleTest {

    public static final String USERNAME = "nithyamani3";
    public static final String AUTOMATE_KEY = "P4JKysg5WuchQxBfKQu1";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    public static void main(String[] args) throws Exception {

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("username", USERNAME);
        capabilities.setCapability("accessKey", AUTOMATE_KEY);

        capabilities.setCapability("name","VPW-UI-Tests");
        capabilities.setCapability("platform","ANDROID");
        capabilities.setCapability("device","Google Pixel 3");
        capabilities.setCapability("realMobile", "true");
        capabilities.setCapability("browserstack.selenium.jar.version", "3.5.2");
        capabilities.setCapability("browserstack.selenium_version", "3.5.2");

        capabilities.setCapability("realMobile", "true");

        WebDriver driver = new RemoteWebDriver(new URL(URL), capabilities);
        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.name("q"));

        element.sendKeys("BrowserStack");
        element.submit();
        System.out.println(driver.getTitle());

        //REST API
        String title = driver.getTitle();

        /*SessionId session = ((RemoteWebDriver) driver).getSessionId();
        if(title.equals("BrowserStack - Google Search"))
            TestStatus.mark(session,"passed");
        else
            TestStatus.mark(session,"failed");*/

        driver.quit();

    }
}














